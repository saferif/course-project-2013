package ru.insafkin.algorithms;

import ru.insafkin.algorithms.misc.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.lang.System;
import java.rmi.RemoteException;

/**
 * Реализация алгоритма художника.
 */
public class MainPanel extends JPanel {
    private Scene3D scene;
    private Color[] sortedColors;

    private int width = 0, height = 0;
    private double angleX = 0;
    private double angleY = 0;
    private double angleZ = 0;
    private double dX = 0;
    private double dY = 0;
    private double dZ = 1.5;

    private Matrix perspective = null;

    private double[][] pts = new double[3][3];
    private double[] point = new double[2];

    /**
     * Конструктор класса.
     * @param scene модель сцены.
     */
    public MainPanel(Scene3D scene) {
        this.scene = scene;
        setFocusable(true);
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);    //To change body of overridden methods use File | Settings | File Templates.
                if (e.getKeyCode() == KeyEvent.VK_A) MainPanel.this.angleX += Math.PI / 2 / 25;
                if (e.getKeyCode() == KeyEvent.VK_Q) MainPanel.this.angleX -= Math.PI / 2 / 25;

                if (e.getKeyCode() == KeyEvent.VK_W) MainPanel.this.angleY += Math.PI / 2 / 25;
                if (e.getKeyCode() == KeyEvent.VK_S) MainPanel.this.angleY -= Math.PI / 2 / 25;

                if (e.getKeyCode() == KeyEvent.VK_D) MainPanel.this.angleZ += Math.PI / 2 / 25;
                if (e.getKeyCode() == KeyEvent.VK_E) MainPanel.this.angleZ -= Math.PI / 2 / 25;

                if (e.getKeyCode() == KeyEvent.VK_LEFT) MainPanel.this.dX += .1;
                if (e.getKeyCode() == KeyEvent.VK_RIGHT) MainPanel.this.dX -= .1;

                if (e.getKeyCode() == KeyEvent.VK_DOWN) MainPanel.this.dY += .1;
                if (e.getKeyCode() == KeyEvent.VK_UP) MainPanel.this.dY -= .1;

                if (e.getKeyCode() == KeyEvent.VK_R) MainPanel.this.dZ += .1;
                if (e.getKeyCode() == KeyEvent.VK_F) MainPanel.this.dZ -= .1;
                MainPanel.this.repaint();
            }
        });
    }

    /**
     * Сортирует треугольники на сцене.
     * @param scene Сцена.
     */
    private void sortTriangles(Matrix scene) {
        try {
            sortedColors = this.scene.getColors().clone();
        } catch (RemoteException ex) { ex.printStackTrace(); System.exit(-1); }

        double[] masTriangle = new double[scene.getM() / 3];
        for (int i = 0; i < masTriangle.length; ++i) {
            masTriangle[i] = (scene.get(2, i * 3) + scene.get(2, i * 3 + 1) + scene.get(2, i * 3 + 2)) / 3.0;
            //masTriangle[i] = Math.max(scene.get(2, i * 3), Math.max(scene.get(2, i * 3 + 1), scene.get(2, i * 3 + 2)));
        }
        for (int i = 0; i < masTriangle.length - 1; ++i) {
            for (int j = 0; j < masTriangle.length - 1; ++j) {
                if (masTriangle[j] < masTriangle[j + 1]) {
                    double t = masTriangle[j];
                    masTriangle[j] = masTriangle[j + 1];
                    masTriangle[j + 1] = t;

                    Color tmp = sortedColors[j];
                    sortedColors[j] = sortedColors[j + 1];
                    sortedColors[j + 1] = tmp;

                    scene.swapColumns(j * 3, (j + 1) * 3);
                    scene.swapColumns(j * 3 + 1, (j + 1) * 3 + 1);
                    scene.swapColumns(j * 3 + 2, (j + 1) * 3 + 2);
                }
            }
        }
    }

    /**
     * Пересчитывает размеры поля отображения.
     */
    private void revalidateDimensions() {
        width = getWidth();
        height = getHeight();
        perspective = Matrix.getPerspective(90, width * 1. / height, 0.1, 100);
    }

    /**
     * Рисует сцену на панели.
     * @param g_ Экземпляр объекта работы с графикой.
     */
    @Override
    protected void paintComponent(Graphics g_) {
        super.paintComponent(g_);
        Graphics2D g = (Graphics2D)g_;
        setBackground(Color.BLACK);

        if (width != getWidth() || height != getHeight()) {
            revalidateDimensions();
        }

        Matrix rotation = Matrix.getRotationX(angleX).multiply(Matrix.getRotationY(angleY)).multiply(Matrix.getRotationZ(angleZ));
        Matrix translation = Matrix.getTranslation(dX, dY, dZ);
        Matrix mvMatrix = translation.multiply(rotation);
        Matrix transformedScene = null;
        try {
            transformedScene = mvMatrix.multiply(new Matrix(scene.getScene()));
        } catch (RemoteException ex) { ex.printStackTrace(); System.exit(-1); }

        sortTriangles(transformedScene);
        Matrix points = perspective.multiply(transformedScene).normalizeThis();
        for (int j = 0; j < points.getM(); j += 3) {
            for (int i = 0; i < 3; ++i) {
                Matrix.getPointMap(point, points.get(0, i + j), points.get(1, i + j), 0, 0, width, height);
                pts[i][0] = point[0];
                pts[i][1] = point[1];
                pts[i][2] = points.get(2, i + j);
            }
            drawTriangle(pts, sortedColors[j / 3], g);
        }
    }

    /**
     * Сортирует точки треугольника.
     * @param order Массив с результатом.
     * @param points Массив точек.
     */
    private void sortPoints(int[] order, double[][] points) {
        if (points[0][1] < points[1][1])
        {
            if (points[0][1] < points[2][1])
            {
                order[0] = 0;
            }
            else
            {
                order[0] = 2;
            }
        }
        else
        {
            if (points[1][1] < points[2][1])
            {
                order[0] = 1;
            }
            else
            {
                order[0] = 2;
            }
        }

        if (points[0][1] > points[1][1])
        {
            if (points[0][1] > points[2][1])
            {
                order[2] = 0;
            }
            else
            {
                order[2] = 2;
            }
        }
        else
        {
            if (points[1][1] > points[2][1])
            {
                order[2] = 1;
            }
            else
            {
                order[2] = 2;
            }
        }

        order[1] = 3 - (order[0] + order[2]);
    }

    /**
     * Рисует треугольник.
     * @param points Вершины треугольника.
     * @param color Цвет реугольника.
     * @param g Экземпляр объекта работы с графикой.
     */
    private void drawTriangle(double[][] points, Color color, Graphics2D g) {

        final double eps = 1e-3;
        g.setPaint(color);

        int[] order = new int[3];
        sortPoints(order, points);

        if (points[order[2]][1] - points[order[0]][1] < eps) return;

        double d0 = (points[order[1]][0] - points[order[0]][0]) / ((int)points[order[1]][1] - (int)points[order[0]][1] + 1);
        double dz0 = (points[order[1]][2] - points[order[0]][2]) / ((int)points[order[1]][1] - (int)points[order[0]][1] + 1);
        double d1 = (points[order[2]][0] - points[order[0]][0]) / ((int)points[order[2]][1] - (int)points[order[0]][1] + 1);
        double dz1 = (points[order[2]][2] - points[order[0]][2]) / ((int)points[order[2]][1] - (int)points[order[0]][1] + 1);

        double x0, x1, z0, z1;
        if (points[order[1]][1] - points[order[0]][1] >= eps) {
            x0 = x1 = points[order[0]][0];
            z0 = z1 = points[order[0]][2];
            int miny = (int)points[order[0]][1];
            int maxy = (int)points[order[1]][1];
            for (int y = miny; y <= maxy; ++y) {
                long minx, maxx;
                double minz, maxz;
                if (x0 < x1) {
                    minx = Math.round(x0);
                    maxx = Math.round(x1);
                    minz = z0;
                    maxz = z1;
                }
                else {
                    minx = Math.round(x1);
                    maxx = Math.round(x0);
                    minz = z1;
                    maxz = z0;
                }
                double dz = (maxz - minz) / (maxx - minx);
                double z = minz;
                for (long x = minx; x <= maxx; ++x, z += dz) {
                    g.drawLine((int)x, y, (int)x, y);
                }
                x0 += d0; x1 += d1;
                z0 += dz0; z1 += dz1;
            }
        } else {
            x0 = points[order[1]][0];
            x1 = points[order[0]][0];
            z0 = points[order[1]][2];
            z1 = points[order[0]][2];
        }
        if (points[order[2]][1] - points[order[1]][1] < eps) return;
        d0 = (points[order[2]][0] - points[order[1]][0]) / ((int)points[order[2]][1] - (int)points[order[1]][1] + 1);
        dz0 = (points[order[2]][2] - points[order[1]][2]) / ((int)points[order[2]][1] - (int)points[order[1]][1] + 1);
        int miny = (int)points[order[1]][1];
        int maxy = (int)points[order[2]][1];
        for (int y = miny; y <= maxy; ++y) {
            long minx, maxx;
            double minz, maxz;
            if (x0 < x1) {
                minx = Math.round(x0);
                maxx = Math.round(x1);
                minz = z0;
                maxz = z1;
            }
            else {
                minx = Math.round(x1);
                maxx = Math.round(x0);
                minz = z1;
                maxz = z0;
            }
            double dz = (maxz - minz) / (maxx - minx);
            double z = minz;
            for (long x = minx; x <= maxx; ++x, z += dz) {
                g.drawLine((int)x, y, (int)x, y);
            }
            x0 += d0; x1 += d1;
            z0 += dz0; z1 += dz1;
        }
    }
}
