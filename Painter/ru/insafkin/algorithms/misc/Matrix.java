package ru.insafkin.algorithms.misc;

/**
 * Матрица.
 */
public class Matrix {
    private int N = 0, M = 0;
    private double[][] mat = null;

    /**
     * Конструктор.
     * @param matrix Описание матрицы в виде массива.
     */
    public Matrix(double[][] matrix) {
        mat = matrix;
        N = mat.length;
        if (N == 0) M = 0; else M = mat[0].length;
    }

    /**
     * Конструктор.
     * @param N Количество строк.
     * @param M Количество столбцов.
     */
    public Matrix(int N, int M) {
        mat = new double[N][M];
        this.N = N;
        this.M = M;
    }

    /**
     * Вычисляет отображение точки в однородных координатах в экранные координаты.
     * @param result Массив с результатом.
     * @param x X-координата точки.
     * @param y Y-координата точки в однородных координатах.
     * @param left X-координата левой верхней точки области отображения в экранных координатах.
     * @param top Y-координата левой верхней точки области отображения в экранных координатах.
     * @param width Ширина области отображения.
     * @param height Высота области отображения.
     */
    public static void getPointMap(double[] result, double x, double y, int left, int top, int width, int height) {
        result[0] = left + width * (1 + x) / 2;
        result[1] = top + height * (1 - y) / 2;
    }

    /**
     * Возвращает количество строк в матрице.
     * @return Количество строк в матрице.
     */
    public int getN() { return N; }

    /**
     * Возвращает количество столбцов в матрице.
     * @return Количество столбцов в матрице.
     */
    public int getM() { return M; }

    /**
     * Возвращает матрицу в виде двухмерного массива.
     * @return Матрица в виде двухмерного массива.
     */
    public double[][] get() { return mat; }

    /**
     * Получает элемент матрицы.
     * @param I Строка элемента.
     * @param J Столбец элемента.
     * @return Элемент матрицы.
     */
    public double get(int I, int J) { return mat[I][J]; }

    /**
     * Умножает матрицу на заданную.
     * @param matrix Матрица, на которую производится умножение.
     * @return Произведение матриц.
     */
    public Matrix multiply(Matrix matrix) {
        if (M != matrix.N)
            return null;
        Matrix result = new Matrix(N, matrix.M);
        for (int i = 0; i < result.N; ++i)
        {
            for (int j = 0; j < result.M; ++j)
            {
                result.mat[i][j] = 0;
                for (int k = 0; k < M; k++)
                {
                    result.mat[i][j] += mat[i][k] * matrix.mat[k][j];
                }
            }
        }
        return result;
    }

    /**
     * Умножает текущую матрицу на скаляр.
     * @param x Скаляр.
     * @return Произведение.
     */
    public Matrix multiply(double x) {
        for (int i = 0; i < N; ++i)
        {
            for (int j = 0; j < M; ++j)
            {
                mat[i][j] = mat[i][j] * x;
            }
        }
        return this;
    }

    /**
     * Транспонирует текущую матрицу.
     * @return Результат транспонирования.
     */
    public Matrix T() {
        Matrix result = new Matrix(M, N);
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < M; ++j) {
                result.mat[j][i] = mat[i][j];
            }
        }
        return result;
    }

    /**
     * Нормализует текущую матрицу с учетом W-координаты.
     * @return Нормализованная матрица.
     */
    public Matrix normalizeThis() {
        for (int i = 0; i < M; ++i) {
            mat[0][i] /= mat[3][i];
            mat[1][i] /= mat[3][i];
            mat[2][i] /= mat[3][i];
            mat[3][i] /= mat[3][i];
        }
        return this;
    }

    /**
     * Меняет столбцы местами.
     * @param a Номер первого столбца.
     * @param b Номер второго столбца.
     */
    public void swapColumns(int a, int b) {
        for (int i = 0; i < N; ++i) {
            double t = mat[i][a];
            mat[i][a] = mat[i][b];
            mat[i][b] = t;
        }
    }

    /**
     * Меняет строки местами.
     * @param a Номер первой строки.
     * @param b Номер второй строки.
     */
    public void swapRows(int a, int b) {
        for (int i = 0; i < M; ++i) {
            double t = mat[a][i];
            mat[a][i] = mat[b][i];
            mat[b][i] = t;
        }
    }

    /**
     * Возвращает еденичную матрицу 4x4.
     * @return Еденичная матрица.
     */
    public static Matrix getIdentity() {
        return new Matrix(new double[][] {
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
        });
    }

    /**
     * Строит матрицу переноса.
     * @param dx Велечина переноса по оси X.
     * @param dy Велечина переноса по оси Y.
     * @param dz Велечина переноса по оси Z.
     * @return Матрица переноса.
     */
    public static Matrix getTranslation(double dx, double dy, double dz) {
        return new Matrix(new double[][] {
            {1, 0, 0, dx},
            {0, 1, 0, dy},
            {0, 0, 1, dz},
            {0, 0, 0, 1}
        });
    }

    /**
     * Строит матрицу масштабирования.
     * @param sx Велечина масштабирования по оси X.
     * @param sy Велечина масштабирования по оси Y.
     * @param sz Велечина масштабирования по оси Z.
     * @return Матрица масштабирования.
     */
    public static Matrix getScale(double sx, double sy, double sz) {
        return new Matrix(new double[][] {
                {sx, 0, 0, 0},
                {0, sy, 0, 0},
                {0, 0, sz, 1},
                {0, 0, 0, 1}
        });
    }

    /**
     * Строит матрицу поворота вокруг оси X.
     * @param angle Велечина поворота.
     * @return Матрица поворота.
     */
    public static Matrix getRotationX(double angle) {
        return new Matrix(new double[][] {
                {1, 0, 0, 0},
                {0, Math.cos(angle), -Math.sin(angle), 0},
                {0, Math.sin(angle), Math.cos(angle), 0},
                {0, 0, 0, 1}
        });
    }

    /**
     * Строит матрицу поворота вокруг оси Y.
     * @param angle Велечина поворота.
     * @return Матрица поворота.
     */
    public static Matrix getRotationY(double angle) {
        return new Matrix(new double[][] {
                {Math.cos(angle), 0, Math.sin(angle), 0},
                {0, 1, 0, 0},
                {-Math.sin(angle), 0, Math.cos(angle), 0},
                {0, 0, 0, 1}
        });
    }

    /**
     * Строит матрицу поворота вокруг оси Z.
     * @param angle Велечина поворота.
     * @return Матрица поворота.
     */
    public static Matrix getRotationZ(double angle) {
        return new Matrix(new double[][] {
                {Math.cos(angle), -Math.sin(angle), 0, 0},
                {Math.sin(angle), Math.cos(angle), 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
        });
    }

    /**
     * Строит матрицу проекции по заданным плоскостям отсечения.
     * @param left Координата левой плоскости отсечения.
     * @param right Координата правой плоскости отсечения.
     * @param bottom Координата нижней плоскости отсечения.
     * @param top Координата верхней плоскости отсечения.
     * @param znear Координата ближней плоскости отсечения.
     * @param zfar Координата дальней плоскости отсечения.
     * @return Матрица проекции.
     */
    public static Matrix getFrustum(double left, double right, double bottom, double top, double znear, double zfar) {
        double X = 2 * znear / (right - left);
        double Y = 2 * znear / (top - bottom);
        double A = (right + left) / (right - left);
        double B = (top + bottom) / (top - bottom);
        double C = -(zfar + znear) / (zfar - znear);
        double D = -2 * zfar * znear / (zfar - znear);

        return new Matrix(new double[][]{
                {X, 0, A, 0},
                {0, Y, B, 0},
                {0, 0, C, D},
                {0, 0, -1, 0}
        });
    }

    /**
     * Строит матрицу перспективной проекции.
     * @param fovy Угол обзора по оси Y.
     * @param aspect Отношение ширины области отображения к ее высоте.
     * @param znear Координата ближней плоскости отсечения.
     * @param zfar Координата дальней плоскости отсечения.
     * @return Матрица проекции.
     */
    public static Matrix getPerspective(double fovy, double aspect, double znear, double zfar) {
        double ymax = znear * Math.tan(fovy * Math.PI / 360.0);
        double ymin = -ymax;
        double xmin = ymin * aspect;
        double xmax = ymax * aspect;

        return getFrustum(xmin, xmax, ymin, ymax, znear, zfar);
    }
}
