package ru.insafkin.algorithms.misc;

import java.awt.*;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Модель сцены для отображения
 */
public interface Scene3D extends Remote {
    /**
     * Возвращает список точек сцены
     * @return Список точек сцены
     * @throws RemoteException
     */
    public double[][] getScene() throws RemoteException;

    /**
     * Возвращает массив цветов полигонов на сцене
     * @return Массив цветов полигонов на сцене
     * @throws RemoteException
     */
    public Color[] getColors() throws RemoteException;
}
