package ru.insafkin.algorithms;

import java.awt.*;

/**
 * Z-������.
 */
public class ZBuffer {
    private static final double MAXDIST = -1000;
    private static final double eps = 1e-3;

    private int width, height;
    private Cell[][] buffer;

    /**
     * ����������� Z-�������.
     * @param width ������ Z-�������.
     * @param height ������ Z-�������.
     */
    public ZBuffer(int width, int height) {
        this.width = width;
        this.height = height;

        buffer = new Cell[height][width];
        initBuffer();
    }

    /**
     * �������������� ������.
     */
    private void initBuffer() {
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                buffer[i][j] = new Cell();
            }
        }
    }

    /**
     * ������� ������.
     * @param clearColor ���� �������.
     */
    public void Clear(Color clearColor) {
        for (Cell[] row : buffer) {
            for (Cell cell : row) {
                cell.setZ(MAXDIST);
                cell.setColor(clearColor);
            }
        }
    }

    /**
     * ��������� ����� ������������.
     * @param order ������ � �����������.
     * @param points ������ �����.
     */
    private void sortPoints(int[] order, double[][] points) {
        if (points[0][1] < points[1][1])
        {
            if (points[0][1] < points[2][1])
            {
                order[0] = 0;
            }
            else
            {
                order[0] = 2;
            }
        }
        else
        {
            if (points[1][1] < points[2][1])
            {
                order[0] = 1;
            }
            else
            {
                order[0] = 2;
            }
        }

        if (points[0][1] > points[1][1])
        {
            if (points[0][1] > points[2][1])
            {
                order[2] = 0;
            }
            else
            {
                order[2] = 2;
            }
        }
        else
        {
            if (points[1][1] > points[2][1])
            {
                order[2] = 1;
            }
            else
            {
                order[2] = 2;
            }
        }

        order[1] = 3 - (order[0] + order[2]);
    }

    /**
     * ��������� ����������� � ������.
     * @param points ������� ������������.
     * @param color ���� ������������.
     */
    public void addTriangle(double[][] points, Color color) {
        int[] order = new int[3];
        sortPoints(order, points);

        if (points[order[2]][1] - points[order[0]][1] < eps) return;

        double d0 = (points[order[1]][0] - points[order[0]][0]) / ((int)points[order[1]][1] - (int)points[order[0]][1] + 1);
        double dz0 = (points[order[1]][2] - points[order[0]][2]) / ((int)points[order[1]][1] - (int)points[order[0]][1] + 1);
        double d1 = (points[order[2]][0] - points[order[0]][0]) / ((int)points[order[2]][1] - (int)points[order[0]][1] + 1);
        double dz1 = (points[order[2]][2] - points[order[0]][2]) / ((int)points[order[2]][1] - (int)points[order[0]][1] + 1);

        double x0, x1, z0, z1;
        if (points[order[1]][1] - points[order[0]][1] >= eps) {
            x0 = x1 = points[order[0]][0];
            z0 = z1 = points[order[0]][2];
            int miny = (int)points[order[0]][1];
            int maxy = (int)points[order[1]][1];
            for (int y = miny; y <= maxy; ++y) {
                long minx, maxx;
                double minz, maxz;
                if (x0 < x1) {
                    minx = Math.round(x0);
                    maxx = Math.round(x1);
                    minz = z0;
                    maxz = z1;
                }
                else {
                    minx = Math.round(x1);
                    maxx = Math.round(x0);
                    minz = z1;
                    maxz = z0;
                }
                double dz = (maxz - minz) / (maxx - minx);
                double z = minz;
                for (long x = minx; x <= maxx; ++x, z += dz) {
                    if (x < 0 || x >= width || y < 0 || y >= height) continue;
                    if (buffer[y][(int)x].getZ() - z <= eps) {
                        buffer[y][(int)x].setZ(z);
                        buffer[y][(int)x].setColor(color);
                    }
                }
                x0 += d0; x1 += d1;
                z0 += dz0; z1 += dz1;
            }
        } else {
            x0 = points[order[1]][0];
            x1 = points[order[0]][0];
            z0 = points[order[1]][2];
            z1 = points[order[0]][2];
        }
        if (points[order[2]][1] - points[order[1]][1] < eps) return;
        d0 = (points[order[2]][0] - points[order[1]][0]) / ((int)points[order[2]][1] - (int)points[order[1]][1] + 1);
        dz0 = (points[order[2]][2] - points[order[1]][2]) / ((int)points[order[2]][1] - (int)points[order[1]][1] + 1);
        int miny = (int)points[order[1]][1];
        int maxy = (int)points[order[2]][1];
        for (int y = miny; y <= maxy; ++y) {
            long minx, maxx;
            double minz, maxz;
            if (x0 < x1) {
                minx = Math.round(x0);
                maxx = Math.round(x1);
                minz = z0;
                maxz = z1;
            }
            else {
                minx = Math.round(x1);
                maxx = Math.round(x0);
                minz = z1;
                maxz = z0;
            }
            double dz = (maxz - minz) / (maxx - minx);
            double z = minz;
            for (long x = minx; x <= maxx; ++x, z += dz) {
                if (x < 0 || x >= width || y < 0 || y >= height) continue;
                if (buffer[y][(int)x].getZ() - z <= eps) {
                    buffer[y][(int)x].setZ(z);
                    buffer[y][(int)x].setColor(color);
                }
            }
            x0 += d0; x1 += d1;
            z0 += dz0; z1 += dz1;
        }
    }

    /**
     * ������������ ������.
     * @param g ��������� ������� ������ � ��������.
     */
    public void Draw(Graphics2D g) {
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                g.setColor(buffer[i][j].getColor());
                g.drawLine(j, i, j, i);
            }
        }
    }
}

/**
 * ������ Z-�������.
 */
class Cell {
    private double z;
    private Color color;

    /**
     * ���������� �������� Z-���������� � ������.
     * @return �������� Z-���������� � ������.
     */
    double getZ() {
        return z;
    }

    /**
     * ������ �������� Z-���������� � ������.
     * @param z ����� �������� Z-���������� � ������.
     */
    void setZ(double z) {
        this.z = z;
    }

    /**
     * ���������� �������� ����� � ������.
     * @return �������� ����� � ������.
     */
    Color getColor() {
        return color;
    }

    /**
     * ������ �������� ����� � ������.
     * @param color ����� �������� ����� � ������.
     */
    void setColor(Color color) {
        this.color = color;
    }
}
