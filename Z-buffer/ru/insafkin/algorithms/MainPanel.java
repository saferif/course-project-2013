package ru.insafkin.algorithms;

import ru.insafkin.algorithms.misc.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.lang.System;
import java.rmi.RemoteException;

/**
 * Реализация алгоритма Z-буффер.
 */
public class MainPanel extends JPanel {
    private Scene3D scene;

    private int width = 0, height = 0;
    private double angleX = 0;
    private double angleY = 0;
    private double angleZ = 0;
    private double dX = 0;
    private double dY = 0;
    private double dZ = 1.5;

    private Matrix perspective = null;

    private ZBuffer zBuffer = null;
    private double[][] pts = new double[3][3];
    private double[] point = new double[2];

    /**
     * Конструктор класса.
     * @param scene модель сцены.
     */
    public MainPanel(Scene3D scene) {
        this.scene = scene;
        setFocusable(true);
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);    //To change body of overridden methods use File | Settings | File Templates.
                if (e.getKeyCode() == KeyEvent.VK_A) MainPanel.this.angleX += Math.PI / 2 / 25;
                if (e.getKeyCode() == KeyEvent.VK_Q) MainPanel.this.angleX -= Math.PI / 2 / 25;

                if (e.getKeyCode() == KeyEvent.VK_W) MainPanel.this.angleY += Math.PI / 2 / 25;
                if (e.getKeyCode() == KeyEvent.VK_S) MainPanel.this.angleY -= Math.PI / 2 / 25;

                if (e.getKeyCode() == KeyEvent.VK_D) MainPanel.this.angleZ += Math.PI / 2 / 25;
                if (e.getKeyCode() == KeyEvent.VK_E) MainPanel.this.angleZ -= Math.PI / 2 / 25;

                if (e.getKeyCode() == KeyEvent.VK_LEFT) MainPanel.this.dX += .1;
                if (e.getKeyCode() == KeyEvent.VK_RIGHT) MainPanel.this.dX -= .1;

                if (e.getKeyCode() == KeyEvent.VK_DOWN) MainPanel.this.dY += .1;
                if (e.getKeyCode() == KeyEvent.VK_UP) MainPanel.this.dY -= .1;

                if (e.getKeyCode() == KeyEvent.VK_R) MainPanel.this.dZ += .1;
                if (e.getKeyCode() == KeyEvent.VK_F) MainPanel.this.dZ -= .1;
                MainPanel.this.repaint();
            }
        });
    }

    /**
     * Пересчитывает размеры поля отображения.
     */
    private void revalidateDimensions() {
        width = getWidth();
        height = getHeight();
        perspective = Matrix.getPerspective(90, width * 1. / height, 0.1, 100);
        zBuffer = new ZBuffer(width, height);
    }

    /**
     * Рисует сцену на панели.
     * @param g_ Экземпляр объекта работы с графикой.
     */
    @Override
    protected void paintComponent(Graphics g_) {
        super.paintComponent(g_);
        Graphics2D g = (Graphics2D)g_;

        if (width != getWidth() || height != getHeight()) {
            revalidateDimensions();
        }

        Matrix rotation = Matrix.getRotationX(angleX).multiply(Matrix.getRotationY(angleY)).multiply(Matrix.getRotationZ(angleZ));
        Matrix translation = Matrix.getTranslation(dX, dY, dZ);
        Matrix mvMatrix = translation.multiply(rotation);
        Matrix transformedScene = null;
        try{
            transformedScene = mvMatrix.multiply(new Matrix(scene.getScene()));
        } catch (RemoteException ex) { ex.printStackTrace(); System.exit(-1); }

        Matrix points = perspective.multiply(transformedScene).normalizeThis();
        Color[] colors = null;
        try {
            colors = scene.getColors();
        } catch (RemoteException ex) { ex.printStackTrace(); System.exit(-1); }
        zBuffer.Clear(Color.BLACK);
        for (int j = 0; j < points.getM(); j += 3) {
            for (int i = 0; i < 3; ++i) {
                Matrix.getPointMap(point, points.get(0, i + j), points.get(1, i + j), 0, 0, width, height);
                pts[i][0] = point[0];
                pts[i][1] = point[1];
                pts[i][2] = points.get(2, i + j);
            }
            zBuffer.addTriangle(pts, colors[j / 3]);
        }
        zBuffer.Draw(g);
    }
}
