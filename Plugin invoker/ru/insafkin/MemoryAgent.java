package ru.insafkin;

import java.lang.instrument.Instrumentation;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Основной класс Java-агента для подсчета количества выделенного под объект памяти.
 */
public class MemoryAgent {
    private static volatile Instrumentation instrumentation;

    /**
     * Точка входа.
     * @param args Аргументы.
     * @param instrumentation Инструментарий.
     */
    public static void premain(String args, Instrumentation instrumentation) { MemoryAgent.instrumentation = instrumentation; }

    /**
     * Считает "глубокий" размер объекта.
     * @param obj Объект.
     * @return Размер объекта.
     */
    public static long deepSizeOf(Object obj) {
        Map visited = new IdentityHashMap();
        Stack stack = new Stack();
        stack.push(obj);

        long result = 0;
        do {
            result += internalSizeOf(stack.pop(), stack, visited);
        } while (!stack.isEmpty());
        return result;
    }

    /**
     * Расчитывает размер объекта и добавляет его зависимости в стек для дальнейшей обработки.
     * @param obj Объект.
     * @param stack Стек обработки зависимостей.
     * @param visited Список уже рассмотренных зависимостей.
     * @return Размер объекта.
     */
    private static long internalSizeOf(
            Object obj, Stack stack, Map visited) {
        if (skipObject(obj, visited)) {
            return 0;
        }

        Class clazz = obj.getClass();
        if (clazz.isArray()) {
            addArrayElementsToStack(clazz, obj, stack);
        } else {
            while (clazz != null) {
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    if (!Modifier.isStatic(field.getModifiers())
                            && !field.getType().isPrimitive()) {
                        field.setAccessible(true);
                        try {
                            stack.add(field.get(obj));
                        } catch (IllegalAccessException ex) {
                            throw new RuntimeException(ex);
                        }
                    }
                }
                clazz = clazz.getSuperclass();
            }
        }
        visited.put(obj, null);
        return sizeOf(obj);
    }

    /**
     * Определяет пропускать обработку переданного объекта или нет.
     * @param obj Объект.
     * @param visited Список уже рассмотренных зависимостей.
     * @return Пропускать обработку переданного объекта или нет.
     */
    private static boolean skipObject(Object obj, Map visited) {
        return obj == null
                || visited.containsKey(obj)
                || isSharedFlyweight(obj);
    }

    /**
     * Добавляет все элементы массива в стек для дальнейшкй обработки.
     * @param clazz Класс массива.
     * @param obj Экземпляр массива.
     * @param stack Стек обработки зависимостей.
     */
    private static void addArrayElementsToStack(
            Class clazz, Object obj, Stack stack) {
        if (!clazz.getComponentType().isPrimitive()) {
            int length = Array.getLength(obj);
            for (int i = 0; i < length; i++) {
                stack.add(Array.get(obj, i));
            }
        }
    }

    /**
     * Определяет является ли переданный объект закэшированным объектом.
     * @param obj Объект.
     * @return Является ли переданный объект закэшированным объектом.
     */
    private static boolean isSharedFlyweight(Object obj) {
        if (obj instanceof Comparable) {
            if (obj instanceof Enum) {
                return true;
            } else if (obj instanceof String) {
                return (obj == ((String) obj).intern());
            } else if (obj instanceof Boolean) {
                return (obj == Boolean.TRUE || obj == Boolean.FALSE);
            } else if (obj instanceof Integer) {
                return (obj == Integer.valueOf((Integer) obj));
            } else if (obj instanceof Short) {
                return (obj == Short.valueOf((Short) obj));
            } else if (obj instanceof Byte) {
                return (obj == Byte.valueOf((Byte) obj));
            } else if (obj instanceof Long) {
                return (obj == Long.valueOf((Long) obj));
            } else if (obj instanceof Character) {
                return (obj == Character.valueOf((Character) obj));
            }
        }
        return false;
    }

    /**
     * Возвращает размер объекта.
     * @param obj Объект.
     * @return Размер объекта.
     */
    private static long sizeOf(Object obj) {
        if (instrumentation == null) {
            throw new IllegalStateException(
                    "Instrumentation environment not initialised.");
        }
        if (isSharedFlyweight(obj)) {
            return 0;
        }
        return instrumentation.getObjectSize(obj);
    }
}
