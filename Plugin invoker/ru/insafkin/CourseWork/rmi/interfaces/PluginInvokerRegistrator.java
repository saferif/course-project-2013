package ru.insafkin.CourseWork.rmi.interfaces;

import ru.insafkin.algorithms.misc.Scene3D;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Интерфейс регистратора подключаемых модулей.
 */
public interface PluginInvokerRegistrator extends Remote {
    /**
     * Регистрирует подключаемый модуль.
     * @param pluginName Название подключаемого модуля.
     * @param pluginInvoker Экземпляром интерфейса для общения с подключаемым модулем.
     * @return Модель сцены.
     * @throws RemoteException
     */
    public Scene3D register(String pluginName, PluginInvoker pluginInvoker) throws RemoteException;

    /**
     * Сообщает о завершении работы подключаемого модуля.
     * @param pluginName Название подключаемого модуля.
     * @throws RemoteException
     */
    public void pluginClosed(String pluginName) throws RemoteException;
}
