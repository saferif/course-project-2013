package ru.insafkin.CourseWork.rmi.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Интерфейс для общения с подключаемым модулем.
 */
public interface PluginInvoker extends Remote {
    /**
     * Получает текущий FPS.
     * @return FPS.
     * @throws RemoteException
     */
    public long getFPS() throws RemoteException;

    /**
     * Получает количество выделенной памяти.
     * @return Количество выделенной памяти.
     * @throws RemoteException
     */
    public double getMemory() throws RemoteException;
}
