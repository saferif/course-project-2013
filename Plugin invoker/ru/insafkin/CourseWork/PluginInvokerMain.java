package ru.insafkin.CourseWork;

import ru.insafkin.CourseWork.rmi.interfaces.PluginInvoker;
import ru.insafkin.CourseWork.rmi.interfaces.PluginInvokerRegistrator;
import ru.insafkin.MemoryAgent;
import ru.insafkin.algorithms.misc.Scene3D;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.net.URLClassLoader;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Главный класс инвокера.
 */
public class PluginInvokerMain extends JFrame implements PluginInvoker {
    private String pluginName;
    private JPanel plugin;
    private PluginInvokerRegistrator pluginInvokerRegistrator;
    private PluginInvoker pluginInvokerStub = null;
    private Scene3D scene3D;

    private int frames = 0;
    private long starttime = 0;
    private long FPS = 0;

    /**
     * Пересчитывает FPS.
     */
    private void countFPS() {
        ++frames;
        long time = System.currentTimeMillis() / 1000;
        if (time != starttime) {
            starttime = time;
            FPS = frames;
            frames = 0;
        }
    }

    /**
     * Получает экземпляр регистратора подключаемых модулей.
     * @param pluginName Название подключаемого модуля.
     * @throws Exception
     */
    private void getRegistrator(String pluginName) throws Exception {
        Registry registry = LocateRegistry.getRegistry(null, 8546);
        pluginInvokerRegistrator = (PluginInvokerRegistrator) registry.lookup("pluginInvokerRegistrator");
        pluginInvokerStub = (PluginInvoker) UnicastRemoteObject.exportObject(this, 0);
        scene3D = pluginInvokerRegistrator.register(pluginName, pluginInvokerStub);
    }

    /**
     * Инициализирует подключаемый модуль.
     * @param pluginName Название подключаемого модуля.
     * @throws Exception
     */
    private void initPlugin(String pluginName) throws Exception {
        this.pluginName = pluginName;
        String pluginFile = pluginName + ".jar";
        ClassLoader classLoader = new URLClassLoader(new URL[]{ new File("plugins" + File.separator + pluginFile).toURI().toURL() }, PluginInvokerMain.class.getClassLoader());
        Class pluginClass = Class.forName("ru.insafkin.algorithms.MainPanel", true, classLoader);
        Constructor<JPanel> constructor = pluginClass.getConstructor(Scene3D.class);
        plugin = constructor.newInstance(scene3D);
    }

    /**
     * Инициализирует графический интерфейс.
     */
    private void initGUI() {
        setTitle(pluginName);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        JPanel p = new JPanel() {
            @Override
            protected void paintChildren(Graphics g) {
                super.paintChildren(g);
                countFPS();
                PluginInvokerMain.this.repaint();
            }
        };
        p.setLayout(new BorderLayout());
        p.add(plugin, BorderLayout.CENTER);
        getContentPane().add(p);
        pack();
        Insets insets = getInsets();
        setSize(640 + insets.left + insets.right, 480 + insets.top + insets.bottom);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                try {
                    pluginInvokerRegistrator.pluginClosed(PluginInvokerMain.this.pluginName);
                } catch (RemoteException ex) { ex.printStackTrace(); System.exit(-1); }
            }
        });
    }

    /**
     * Конструктор инвокера.
     * @param pluginName Название подключаемого модуля.
     */
    public PluginInvokerMain(String pluginName) {
        try {
            getRegistrator(pluginName);
            initPlugin(pluginName);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        initGUI();
        setVisible(true);
    }

    /**
     * Получает текущий FPS.
     * @return FPS.
     * @throws RemoteException
     */
    @Override
    public long getFPS() {
        return FPS;
    }

    /**
     * Получает количество выделенной памяти.
     * @return Количество выделенной памяти.
     * @throws RemoteException
     */
    @Override
    public double getMemory() {
        return MemoryAgent.deepSizeOf(plugin) / (1024.0 * 1024.0);
    }

    /**
     * Точка входа.
     * @param args Аргументы коммандной строки.
     */
    public static void main(String[] args) throws Exception {
        final String pluginName = args[0];
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new PluginInvokerMain(pluginName);
            }
        });
    }
}
