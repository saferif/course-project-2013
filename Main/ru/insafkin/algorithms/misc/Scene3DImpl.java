package ru.insafkin.algorithms.misc;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.*;
import java.io.File;

/**
 * Реализация модели сцены.
 */
public class Scene3DImpl implements Scene3D {

    private double[][] scene = null;
    private Color[] colors = null;

    /**
     * Конструирует пустую модель сцены.
     */
    public Scene3DImpl(){}

    /**
     * Загружает сцену из заданного файла.
     * @param file Файл с описанием сцены.
     * @return Модель сцены.
     */
    public Scene3DImpl loadScene(File file) {
        try {
            Element root = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file).getDocumentElement();
            NodeList triangles = root.getChildNodes();
            int n = triangles.getLength();
            scene = new double[4][(n / 2) * 3];
            colors = new Color[n / 2];
            for (int i = 0; i < n; ++i) {
                Node node = triangles.item(i);
                if (!(node instanceof Element)) continue;
                Element triangle = (Element)node;
                if (!triangle.getTagName().equals("triangle")) continue;

                String color = triangle.getAttribute("color");
                colors[i / 2] = Color.decode(color);

                int pointIndex = 0;
                NodeList points = triangle.getChildNodes();
                for (int j = 0; j < points.getLength(); ++j) {
                    Node node1 = points.item(j);
                    if (!(node1 instanceof Element)) continue;
                    Element child = (Element)node1;
                    if (!child.getTagName().equals("point")) continue;
                    String[] point = child.getTextContent().split(";");
                    scene[0][i / 2 * 3 + pointIndex] = Double.parseDouble(point[0]);
                    scene[1][i / 2 * 3 + pointIndex] = Double.parseDouble(point[1]);
                    scene[2][i / 2 * 3 + pointIndex] = Double.parseDouble(point[2]);
                    scene[3][i / 2 * 3 + pointIndex] = 1.;
                    ++pointIndex;
                }
            }
        }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Ошибка", JOptionPane.ERROR_MESSAGE);
            return null;
        }
        return this;
    }

    /**
     * Возвращает список точек сцены.
     * @return Список точек сцены.
     */
    public double[][] getScene() {
        return scene;
    }

    /**
     * Возвращает массив цветов полигонов на сцене.
     * @return Массив цветов полигонов на сцене.
     */
    public Color[] getColors() {
        return colors;
    }
}
