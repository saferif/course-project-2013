package ru.insafkin.CourseWork;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import ru.insafkin.CourseWork.misc.ProcessManager;
import ru.insafkin.CourseWork.rmi.RMIThread;

/**
 * Основной класс приложения.
 */
public class Main extends Application {
    /**
     * Точка входа.
     * @param args Аргументы коммандной строки.
     */
    public static void main(String[] args) { launch(args); }

    /**
     * Запускает приложение.
     * @param primaryStage экземпляр главгого stage приложения.
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("fxmls/form.fxml"));
        Scene scene = new Scene(root);
        primaryStage.getIcons().add(new Image(getClass().getResource("images/icon.gif").toString()));
        primaryStage.setTitle("Сравнение алгоритмов отсечения невидимых граней");
        primaryStage.setScene(scene);
        primaryStage.setMinWidth(800);
        primaryStage.setMinHeight(600);
        primaryStage.setOnHiding(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent windowEvent) {
                RMIThread.getInstance(null).stop();
                ProcessManager.getInstance().deleteAll();
                System.exit(0);
            }
        });
        primaryStage.show();
    }
}
