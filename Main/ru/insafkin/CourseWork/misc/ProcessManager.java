package ru.insafkin.CourseWork.misc;

import java.util.HashMap;
import java.util.Map;

/**
 * Менеджер процессов подключаемых модулей.
 */
public class ProcessManager {
    private static ProcessManager instance = null;

    /**
     * Возвращает экземпляр менеджера.
     * @return Экземпляр менеджера.
     */
    public static ProcessManager getInstance() {
        if (instance == null) {
            instance = new ProcessManager();
        }
        return instance;
    }

    /**
     * Констректор менеджера.
     */
    private ProcessManager(){}
    private Map<String, Process> processes = new HashMap<String, Process>();

    /**
     * Добавляет процесс в список контролтруемых.
     * @param name Название подключаемого модуля.
     * @param process Экземпляр процесса.
     */
    public void add(String name, Process process) {
        processes.put(name, process);
    }

    /**
     * Завершает процесс, связанный с указанным подключаемым модулем.
     * @param name Название подключаемого модуля.
     */
    public void delete(String name) {
        final Process process = processes.get(name);
        if (process != null) {
            processes.remove(process);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    synchronized (instance) {
                        process.destroy();
                    }
                }
            }).start();
        }
    }

    /**
     * Завершает все контролируемые процессы.
     */
    public void deleteAll() {
        for (Process process : processes.values()) {
            process.destroy();
        }
    }
}
