package ru.insafkin.CourseWork.rmi;

import javafx.application.Platform;
import ru.insafkin.CourseWork.controllers.ContentController;
import ru.insafkin.CourseWork.rmi.interfaces.PluginInvoker;
import ru.insafkin.CourseWork.rmi.interfaces.PluginInvokerRegistrator;
import ru.insafkin.algorithms.misc.Scene3D;

/**
 * Реализация регистратора подключаемых модулей.
 */
public class PluginInvokerRegistratorImpl implements PluginInvokerRegistrator {
    private ContentController contentController;

    /**
     * Конструктор регистратора подключаемых модулей.
     * @param controller Котроллер основнего содиржимого главного окна.
     */
    public PluginInvokerRegistratorImpl(ContentController controller) {
        contentController = controller;
    }

    /**
     * Регистрирует подключаемый модуль.
     * @param pluginName Название подключаемого модуля.
     * @param pluginInvoker Экземпляром интерфейса для общения с подключаемым модулем.
     * @return Модель сцены.
     */
    @Override
    public Scene3D register(String pluginName, PluginInvoker pluginInvoker) {
        return contentController.registerInvoker(pluginName, pluginInvoker);
    }

    /**
     * Сообщает о завершении работы подключаемого модуля.
     * @param pluginName Название подключаемого модуля.
     */
    @Override
    public void pluginClosed(final String pluginName) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                contentController.pluginClosed(pluginName);
            }
        });
    }
}
