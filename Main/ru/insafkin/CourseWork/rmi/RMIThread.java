package ru.insafkin.CourseWork.rmi;

import ru.insafkin.CourseWork.controllers.ContentController;
import ru.insafkin.CourseWork.rmi.interfaces.PluginInvokerRegistrator;

import javax.swing.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Поток RMI.
 */
public class RMIThread extends Thread {
    private static RMIThread instance = null;
    private ContentController contentController;
    private Registry registry = null;
    private PluginInvokerRegistrator stub = null;

    /**
     * Конструктор потока RMI.
     * @param controller Котроллер основнего содиржимого главного окна.
     */
    private RMIThread(ContentController controller){
        contentController = controller;
    }

    /**
     * Возвращает экземпляр потока RMI.
     * @param controller Котроллер основнего содиржимого главного окна.
     * @return Экземпляр потока RMI.
     */
    public static RMIThread getInstance(ContentController controller) {
        if (instance == null) {
            instance = new RMIThread(controller);
        }
        return instance;
    }

    /**
     * Создает реестр имен RMI.
     * @param port Номер порта для обслуживания клиентов.
     * @return Экземпляр потока RMI.
     * @throws Exception
     */
    public RMIThread createRegisrty(int port) throws Exception {
        PluginInvokerRegistratorImpl registrator = new PluginInvokerRegistratorImpl(contentController);
        stub = (PluginInvokerRegistrator) UnicastRemoteObject.exportObject(registrator, 0);
        registry = LocateRegistry.createRegistry(port);
        return this;
    }

    /**
     * Реализует функциональность потока.
     */
    @Override
    public void run() {
        super.run();
        try {
            registry.bind("pluginInvokerRegistrator", stub);
        } catch (Exception ex) {System.exit(-1);}
    }
}
