package ru.insafkin.CourseWork.controllers;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import ru.insafkin.CourseWork.misc.ProcessManager;
import ru.insafkin.CourseWork.rmi.RMIThread;
import ru.insafkin.CourseWork.rmi.interfaces.PluginInvoker;
import ru.insafkin.algorithms.misc.Scene3D;
import ru.insafkin.algorithms.misc.Scene3DImpl;

import javax.swing.*;
import java.io.File;
import java.net.URL;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

/**
 * Котроллер основнего содиржимого главного окна.
 */
public class ContentController implements Initializable {
    @FXML private VBox pluginListVBox;
    @FXML private VBox chartVBox;
    @FXML private AnchorPane contentPane;

    private Scene3DImpl scene3D = new Scene3DImpl();
    private Scene3D scene3DStub = null;

    private Map<String, CheckBox> algoCheckBoxMap = new HashMap<String, CheckBox>();
    private Map<String, PluginInvoker> algoInvokerMap = new HashMap<String, PluginInvoker>();
    private Map<String, XYChart.Series<Number, Number>[]> algoSeries = new HashMap<String, XYChart.Series<Number, Number>[]>();

    private LineChart<Number, Number> fpsChart = null, memoryChart = null;
    private Timeline animation = null;

    private long time = 0;

    /**
     * Задает сцену для отображения.
     * @param file Файл с описанием сцены.
     */
    public void setScene3D(File file) {
        if (scene3D.loadScene(file) == null) return;
        pluginListVBox.setDisable(false);
    }

    /**
     * Возвращает ссылку на экземпляр анимации рисования графиков.
     * @return Ссылка на экземпляр анимации рисования графиков.
     */
    public Timeline getAnimation() {
        return animation;
    }

    /**
     * Возвращает список доступных подключаемых модулей.
     * @param folder Файл-дирректория, в которой будет производится поиск подключамых модулей.
     * @return Спосок доступных подключаемых модулей.
     */
    private List<File> listPlugins(final File folder) {
        List<File> result = new ArrayList<File>();
        try {
            for (final File file : folder.listFiles()) {
                if (!file.isDirectory() && file.getName().endsWith(".jar")) {
                    result.add(file);
                }
            }
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return result;
    }

    /**
     * Запускает подключаемый модудь.
     * @param pluginName Название подключаемого модуля.
     */
    private void runPlugin(String pluginName) {
        String javaHome = System.getProperty("java.home");
        String javaBin = javaHome + File.separator + "bin" + File.separator + "javaw";
        String classPath = System.getProperty("java.class.path");
        String invokerJar = "bin" + File.separator + "pluginInvoker.jar";
        String agentJar = "bin" + File.separator + "memoryagent.jar";
        ProcessBuilder builder = new ProcessBuilder(javaBin, "-cp", classPath, "-javaagent:" + agentJar, "-jar", invokerJar, pluginName);
        try {
            ProcessManager.getInstance().add(pluginName, builder.start());
        } catch (Exception ex) { System.exit(-1); }
    }

    /**
     * Инициализирует список подключаемых окон в окне.
     */
    private void initPluginsList() {
        for (File file : listPlugins(new File("plugins"))) {
            String fileName = file.getName();

            final CheckBox checkBox = new CheckBox(fileName.substring(0, fileName.length() - 4));
            checkBox.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    boolean selected = checkBox.isSelected();
                    if (selected) {
                        if (fpsChart.getData().isEmpty()) {
                            ((MenuController)contentPane.getUserData()).resetChartsMenu();
                        }
                        String pluginName = checkBox.getText();
                        final XYChart.Series<Number, Number> fpsSeries = new XYChart.Series<Number, Number>();
                        final XYChart.Series<Number, Number> memorySeries = new XYChart.Series<Number, Number>();
                        fpsSeries.setName(pluginName);
                        memorySeries.setName(pluginName);
                        fpsChart.getData().add(fpsSeries);
                        memoryChart.getData().add(memorySeries);
                        algoSeries.put(pluginName, new XYChart.Series[]{fpsSeries, memorySeries});
                        algoCheckBoxMap.put(pluginName, checkBox);

                        runPlugin(pluginName);

                        if (animation.getStatus() == Animation.Status.STOPPED) {
                            animation.play();
                        }
                    } else {
                        synchronized (ProcessManager.getInstance()) {
                            ContentController.this.pluginClosed(checkBox.getText());
                        }
                    }
                }
            });

            pluginListVBox.getChildren().add(checkBox);
        }
    }

    /**
     * Возвращает графики на начальное состояние.
     */
    private void reinitCharts() {
        NumberAxis axis = (NumberAxis)fpsChart.getXAxis();
        axis.setLowerBound(0);
        axis.setUpperBound(10);
        axis.setTickUnit(10);

        axis = (NumberAxis)memoryChart.getXAxis();
        axis.setLowerBound(0);
        axis.setUpperBound(10);
        axis.setTickUnit(10);
    }

    /**
     * Связывает подключаемый модуль с экземпляром интерфейса для общения с ней.
     * @param pluginName Название подключаемого модуля.
     * @param pluginInvoker Экземпляр интерфейса для общения.
     * @return Модель сцены.
     */
    public Scene3D registerInvoker(String pluginName, PluginInvoker pluginInvoker) {
        algoInvokerMap.put(pluginName, pluginInvoker);
        if (scene3DStub == null) {
            try {
                scene3DStub = (Scene3D) UnicastRemoteObject.exportObject(scene3D, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }
        return scene3DStub;
    }

    /**
     * Обработчик завершения работы подключаемого модуля.
     * @param pluginName Название подключаемого модуля.
     */
    public void pluginClosed(String pluginName) {
        XYChart.Series[] series = algoSeries.get(pluginName);
        fpsChart.getData().remove(series[0]);
        memoryChart.getData().remove(series[1]);
        if (fpsChart.getData().isEmpty()) {
            ((MenuController)contentPane.getUserData()).disableChartsMenu();
            reinitCharts();
            animation.stop();
            time = 0;
        }
        CheckBox checkBox = algoCheckBoxMap.get(pluginName);
        checkBox.setSelected(false);
        algoInvokerMap.remove(pluginName);
        synchronized (ProcessManager.getInstance()) {
            ProcessManager.getInstance().delete(pluginName);
        }
    }

    /**
     * Создает объект для отображения графика FPS.
     * @return Объект для отображения графика FPS.
     */
    private LineChart<Number, Number> createFPSChart() {
        NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("Время (секунды)");
        xAxis.setAutoRanging(false);
        xAxis.setForceZeroInRange(false);
        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("FPS");
        yAxis.setAutoRanging(true);
        LineChart<Number, Number> lc = new LineChart<Number, Number>(xAxis, yAxis);
        lc.setTitle("FPS");
        lc.getData().add(new XYChart.Series<Number, Number>());
        lc.getData().clear();
        return lc;
    }

    /**
     * Создает объект для отображения графика потребления памяти.
     * @return Объект для отображения графика потребления памяти.
     */
    private LineChart<Number, Number> createMemoryChart() {
        NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("Время (секунды)");
        xAxis.setAutoRanging(false);
        xAxis.setForceZeroInRange(false);
        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Память (МиБ)");
        yAxis.setAutoRanging(true);
        LineChart<Number, Number> lc = new LineChart<Number, Number>(xAxis, yAxis);
        lc.setTitle("Количество потребляемой памяти");
        lc.getData().add(new XYChart.Series<Number, Number>());
        lc.getData().clear();
        return lc;
    }

    /**
     * Инициализирует графики.
     */
    private void initCharts() {
        fpsChart = createFPSChart();
        memoryChart = createMemoryChart();
        reinitCharts();
        chartVBox.getChildren().add(fpsChart);
        chartVBox.getChildren().add(memoryChart);
    }

    /**
     * Инициализирует анимацию построения графиков.
     */
    private void initAnimation() {
        animation = new Timeline();
        animation.setCycleCount(Animation.INDEFINITE);
        animation.getKeyFrames().add(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                for (Map.Entry<String, PluginInvoker> entry : algoInvokerMap.entrySet()) {
                    CheckBox checkBox = algoCheckBoxMap.get(entry.getKey());
                    XYChart.Series[] series = algoSeries.get(entry.getKey());
                    if (checkBox.isSelected()) {
                        try {
                            series[0].getData().add(new XYChart.Data<Number, Number>(time, entry.getValue().getFPS()));
                            series[1].getData().add(new XYChart.Data<Number, Number>(time, entry.getValue().getMemory()));
                        } catch (RemoteException ex) {ex.printStackTrace(); System.exit(-1); }

                        NumberAxis axis = (NumberAxis)fpsChart.getXAxis();
                        if (time > (int)axis.getUpperBound()) {
                            axis.setLowerBound(axis.getLowerBound() + 9);
                            axis.setUpperBound(axis.getUpperBound() + 9);
                            axis = (NumberAxis)memoryChart.getXAxis();
                            axis.setLowerBound(axis.getLowerBound() + 9);
                            axis.setUpperBound(axis.getUpperBound() + 9);
                        }
                    }
                }
                ++time;
            }
        }));
    }

    /**
     * Инициализирует механизм RMI.
     */
    private void initRMI() {
        try {
            RMIThread.getInstance(this).createRegisrty(8546).start();
        }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Приложение уже запущено", "Ошибка", JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }
    }

    /**
     * Инициализирует котроллер основнего содиржимого главного окна..
     * @param url The location used to resolve relative paths for the root object, or null if the location is not known.
     * @param resourceBundle The resources used to localize the root object, or null if the root object was not localized.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        initPluginsList();
        initCharts();
        initAnimation();
        initRMI();
    }
}
