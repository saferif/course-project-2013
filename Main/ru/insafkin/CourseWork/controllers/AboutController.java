package ru.insafkin.CourseWork.controllers;

import javafx.event.ActionEvent;
import javafx.scene.Node;

/**
 * Контроллер окна "О программе"
 */
public class AboutController {
    /**
     * Обработчик нажатия кнопки закрытия
     * @param event данные события
     */
    public void closeButtonClicked(ActionEvent event) {
        ((Node)event.getTarget()).getScene().getWindow().hide();
    }
}
