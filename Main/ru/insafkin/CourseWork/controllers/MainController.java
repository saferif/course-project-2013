package ru.insafkin.CourseWork.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Контроллер главного окна.
 */
public class MainController implements Initializable {
    @FXML private ContentController contentPaneController;
    @FXML private MenuController menuBarController;
    @FXML private MenuBar menuBar;
    @FXML private AnchorPane contentPane;

    /**
     * Инициализирует главное окно.
     * @param url The location used to resolve relative paths for the root object, or null if the location is not known.
     * @param resourceBundle The resources used to localize the root object, or null if the root object was not localized.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        menuBar.setUserData(contentPaneController);
        contentPane.setUserData(menuBarController);
    }
}
