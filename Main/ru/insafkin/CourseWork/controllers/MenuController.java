package ru.insafkin.CourseWork.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import ru.insafkin.CourseWork.Main;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Контроллер меню.
 */
public class MenuController implements Initializable {
    @FXML private MenuBar menuBar;
    @FXML private MenuItem openMenuItem;
    @FXML private MenuItem exitMenuItem;
    @FXML private MenuItem pauseMenuItem;
    @FXML private MenuItem resumeMenuItem;

    /**
     * Инициализирует меню.
     * @param url The location used to resolve relative paths for the root object, or null if the location is not known.
     * @param resourceBundle The resources used to localize the root object, or null if the root object was not localized.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        openMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCodeCombination.CONTROL_DOWN));
        exitMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.X, KeyCodeCombination.ALT_DOWN));
        pauseMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.P, KeyCodeCombination.CONTROL_DOWN));
        resumeMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCodeCombination.CONTROL_DOWN));
    }

    /**
     * Обработчик нажатия на кнопку выхода.
     */
    public void exitMenuItemClicked() {
        ((Stage)menuBar.getScene().getWindow()).close();
    }

    /**
     * Обработчик нажатия на кнопку открытия сцены.
     */
    public void openMenuItemClicked() {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(filter);
        fileChooser.setInitialDirectory(new File("scenes"));
        File file = fileChooser.showOpenDialog(menuBar.getScene().getWindow());
        if (file == null) return;
        ((ContentController)menuBar.getUserData()).setScene3D(file);
    }

    /**
     * Сбрасывает состояние элементов меню "Графики" на начальное состояние.
     */
    public void resetChartsMenu() {
        pauseMenuItem.setDisable(false);
        resumeMenuItem.setDisable(true);
    }

    /**
     * Блокирует меню "Графики".
     */
    public void disableChartsMenu() {
        pauseMenuItem.setDisable(true);
        resumeMenuItem.setDisable(true);
    }

    /**
     * Ооработчик нажатия на кнопку паузы.
     */
    public void pauseMenuItemClicked() {
        pauseMenuItem.setDisable(true);
        resumeMenuItem.setDisable(false);
        ((ContentController)menuBar.getUserData()).getAnimation().pause();
    }

    /**
     * Обработчик нажатия на кнопку продолжения.
     */
    public void resumeMenuItemClicked() {
        pauseMenuItem.setDisable(false);
        resumeMenuItem.setDisable(true);
        ((ContentController)menuBar.getUserData()).getAnimation().play();
    }

    /**
     * Обработчик нажатия на кнопку "О программе".
     * @throws Exception
     */
    public void aboutMenuItemClicked() throws Exception {
        Stage stage = new Stage(StageStyle.UTILITY);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(menuBar.getScene().getWindow());
        stage.setTitle("О программе");
        stage.setResizable(false);
        Parent root = FXMLLoader.load(Main.class.getResource("fxmls/about.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.showAndWait();
    }
}
